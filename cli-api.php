<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('max_execution_time', 300); 	//300 seconds = 5 minutes
ini_set('memory_limit', '512M');
ini_set('implicit_flush', 1);

if (!isset($_SERVER['QUERY_STRING'])){
	echo "[]";
	exit;
}
parse_str($_SERVER['QUERY_STRING'], $query_string);
if (!isset($query_string['cmd'])){
	echo 'no command found';
	exit;
}
echo '';
ob_flush();
flush();

exec('rm -f /srv/twint/file.json; touch /srv/twint/file.json');
exec($query_string['cmd'] . ' -o file.json --json');

$file_output = shell_exec('cat /srv/twint/file.json');

$file_output_transformed = '[' . str_replace("}\n{", "},{", $file_output). ']';
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE');
header('Access-Control-Allow-Headers: origin, X-Requested-With');
header('Content-Type: application/json');
echo $file_output_transformed;