#!/usr/bin/env bash

set -x
set -e

DROPLET_NAME=${1:-'my-droplet'}

eval "$(docker-machine env ${DROPLET_NAME})"

docker-machine rm $DROPLET_NAME -f

# Stop and clean all
docker stop $(docker ps -a -q); docker rm $(docker ps -a -q)
docker system prune --all --force

# Rebuild
# docker network create nw_twint
# docker-compose build; docker-compose stop; docker-compose up -d