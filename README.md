# twint-docker

twint-docker is a tunnel to run any unix command inside docker container using twint.


## How to use


- [Install docker](https://docs.docker.com/install)

- Clone the repo `git clone https://gitlab.com/camilasrody/twint-docker.git`

- Run the container

```
cd twint-docker

./run.development.sh
```

This will run twint container exposing port 8081.

You may test it with `curl -I http://127.0.0.1:8081/\?cmd\=ls` and it should return 200.


## How to use in production

- [DigitalOcean Access Token](https://cloud.digitalocean.com/account/api/tokens)

- [DigitalOcean ssh key fingerprint](https://cloud.digitalocean.com/account/security#keys)

Run `./create-do-droplet.sh "twint" "DO_ACCESS_TOKEN" "SSH_KEY_FINGERPRINT"` and it will create DigitalOcean droplet(server) with docker container on it.
