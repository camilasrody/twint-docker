#!/usr/bin/env bash
set -x
set -e

DROPLET_NAME=${1:-'twint-docker'}
DO_ACCESS_TOKEN=$2
SSH_KEY_FINGERPRINT=$3

if [[ -z "$DO_ACCESS_TOKEN" ]]; then
    echo "Must provide DO_ACCESS_TOKEN in environment" 1>&2
    exit 1
fi

if [[ -z "$SSH_KEY_FINGERPRINT" ]]; then
    echo "Must provide SSH_KEY_FINGERPRINT in environment" 1>&2
    exit 1
fi

docker-machine create \
    --driver digitalocean \
    --digitalocean-image="ubuntu-16-04-x64" \
    --digitalocean-access-token "$DO_ACCESS_TOKEN" \
    --digitalocean-region "nyc3" \
    --digitalocean-size "512mb" \
    --digitalocean-ssh-key-fingerprint "$SSH_KEY_FINGERPRINT" \
    "$DROPLET_NAME"

# connects current shell session environment to remote docker-machine environment
eval "$(docker-machine env ${DROPLET_NAME})"

docker network create nw_twint

./run.production.sh