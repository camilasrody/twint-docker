# Usage

## Collect every Tweet containing pineapple from everyone's Tweets.

 - twint -s pineapple

## Display Tweets by verified users that Tweeted about Donald Trump.

 - twint -s "Donald Trump" --verified

## Scrape Tweets from a radius of 1km around a place in Paris and export them to a csv file.

 - twint -g="-25.4335303,-49.2998003km" -o file.csv --csv

## Scrape all the Tweets from user's timeline.

 - twint -u username

## Scrape all Tweets from the user's timeline containing pineapple.

 - twint -u username -s pineapple

## Collect Tweets that were tweeted before 2014.

 - twint -u username --year 2014

## Collect Tweets that were tweeted since 2015-12-20.

 - twint -u username --since 2015-12-20

## Show Tweets that might have phone numbers or email addresses.

 - twint -u username --email --phone

## Scrape a Twitter user's followers.

 - twint -u username --followers

## Scrape who a Twitter user follows.

 - twint -u username --following

## Collect all the Tweets a user has favorited.

 - twint -u username --favorites

## Collect full user information a person follows

 - twint -u username --following --user-full

## Use a slow, but effective method to gather Tweets from a user's profile (Gathers ~3200 Tweets, Including Retweets).

 - twint -u username --profile-full

## Use a quick method to gather the last 900 Tweets (that includes retweets) from a user's profile.

 - twint -u username --retweets

## Resume a search starting from the specified Tweet ID.

 - twint -u username --resume 10940389583058

