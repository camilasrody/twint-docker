FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN=true

RUN \
apt-get update && \
apt-get install -y \
git \
python3-pip \
php

RUN \
pip3 install --upgrade -e git+https://github.com/twintproject/twint.git@origin/master#egg=twint

ENV TIMEZONE=America/Sao_Paulo
RUN echo ${TIMEZONE} > /etc/timezone \
	&& dpkg-reconfigure -f noninteractive tzdata

RUN \
apt-get clean autoclean && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY ./php-prod.ini /usr/local/etc/php/php.ini
COPY ./cli-api.php /twint/cli-api.php

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
VOLUME /twint
WORKDIR /srv/twint